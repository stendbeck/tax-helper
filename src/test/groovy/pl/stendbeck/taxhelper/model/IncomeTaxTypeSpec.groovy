package pl.stendbeck.taxhelper.model

import spock.lang.Specification

class IncomeTaxTypeSpec extends Specification {

    def "method should return 18 (18% from 100) if the base is less than or equal to a first tax threshold"() {
        given:
        def baseAmount = 100
        def firstTaxThreshold1 = 100
        def firstTaxThreshold2 = 150
        when:
        def result1 = IncomeTaxType.PROGRESSIVE.calculateTax(baseAmount, firstTaxThreshold1)
        def result2 = IncomeTaxType.PROGRESSIVE.calculateTax(baseAmount, firstTaxThreshold2)
        then:
        result1 == 18
        result2 == 18
    }

    def "method should return 34 (18% from 100 + 32% from 50) if the base is greater than a first tax threshold"() {
        given:
        def baseAmount = 150
        def firstTaxThreshold = 100
        when:
        def result = IncomeTaxType.PROGRESSIVE.calculateTax(baseAmount, firstTaxThreshold)
        then:
        result == 34
    }

    def "method should return 19 (19% from 100 ) if any case"() {
        given:
        def baseAmount = 100
        def firstTaxThreshold1 = 10
        def firstTaxThreshold2 = 100
        def firstTaxThreshold3 = 1000
        when:
        def result1 = IncomeTaxType.FLAT.calculateTax(baseAmount, firstTaxThreshold1)
        def result2 = IncomeTaxType.FLAT.calculateTax(baseAmount, firstTaxThreshold2)
        def result3 = IncomeTaxType.FLAT.calculateTax(baseAmount, firstTaxThreshold3)
        then:
        result1 == 19
        result2 == 19
        result3 == 19
    }
}
