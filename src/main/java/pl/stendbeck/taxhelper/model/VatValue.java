package pl.stendbeck.taxhelper.model;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum VatValue {
    _23_PERCENT(1.23, 1.0),
    _23_PERCENT_HALF(1.23, 0.5),
    _12_PERCENT(1.12, 1.0),
    _5_PERCENT(1.05, 1.0),
    _0_PERCENT;

    private Double divisor;
    private Double multiplicand;

    VatValue() {
    }

    VatValue(Double divisor, Double multiplicand) {
        this.divisor = divisor;
        this.multiplicand = multiplicand;
    }

    public BigDecimal getVatAmount(BigDecimal amount) {
        if (divisor == null) {
            return BigDecimal.ZERO;
        }
        BigDecimal base = amount.divide(BigDecimal.valueOf(divisor), 2, RoundingMode.HALF_UP);
        BigDecimal fullVat = amount.subtract(base);
        return fullVat.multiply(BigDecimal.valueOf(multiplicand)).setScale(2, RoundingMode.HALF_UP);
    }
}
