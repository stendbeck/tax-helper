package pl.stendbeck.taxhelper.model;

public enum TaxType {
    INCOME_TAX, VAT_TAX
}
