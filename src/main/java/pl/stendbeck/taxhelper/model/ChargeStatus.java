package pl.stendbeck.taxhelper.model;

public enum ChargeStatus {
    OPEN, CLOSED, PAID
}
