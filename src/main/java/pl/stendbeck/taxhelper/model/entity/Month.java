package pl.stendbeck.taxhelper.model.entity;

import lombok.Data;
import pl.stendbeck.taxhelper.model.PeriodStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Entity
@SequenceGenerator(name = "MonthSequence", sequenceName = "seq_month", allocationSize = 1)
public class Month implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MonthSequence")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "year_id")
    private Year year;

    private String name;

    private LocalDate dateFrom;

    private LocalDate dateTo;

    private BigDecimal taxBase = BigDecimal.ZERO;

    private BigDecimal vatAmount = BigDecimal.ZERO;

    @Enumerated(EnumType.STRING)
    private PeriodStatus status;

    public Month(){
    }
}