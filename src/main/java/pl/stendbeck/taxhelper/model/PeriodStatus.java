package pl.stendbeck.taxhelper.model;

public enum PeriodStatus {
    ACTIVE, CLOSED
}
