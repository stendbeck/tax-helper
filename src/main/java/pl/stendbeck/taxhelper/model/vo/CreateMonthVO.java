package pl.stendbeck.taxhelper.model.vo;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class CreateMonthVO {

    @NotNull
    private String name;

    @NotNull
    private LocalDate dateFrom;

    @NotNull
    private LocalDate dateTo;

    @NotNull
    private int year;

    @NotNull
    private String customer;
}
