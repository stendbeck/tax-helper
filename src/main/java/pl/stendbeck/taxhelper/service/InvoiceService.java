package pl.stendbeck.taxhelper.service;

import pl.stendbeck.taxhelper.model.vo.InvoiceVO;

public interface InvoiceService {

    void addInvoice(InvoiceVO invoice);
}
