package pl.stendbeck.taxhelper.service;

import pl.stendbeck.taxhelper.model.entity.Customer;
import pl.stendbeck.taxhelper.model.entity.Month;

import java.time.LocalDate;

public interface ZusContributionService {

    void createZusContributions(Customer customer, Month month);

    void payZus(long zusId, LocalDate date);

}
