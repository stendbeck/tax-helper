package pl.stendbeck.taxhelper.service;

import pl.stendbeck.taxhelper.model.entity.Month;
import pl.stendbeck.taxhelper.model.vo.CreateMonthVO;

import java.time.LocalDate;


public interface PeriodService {

    void createMonth(CreateMonthVO month);

    void closeMonth(long monthId);

    Month findMonthByDate(LocalDate date);

}
