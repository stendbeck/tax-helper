package pl.stendbeck.taxhelper.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import pl.stendbeck.taxhelper.model.IncomeTaxType;
import pl.stendbeck.taxhelper.model.PeriodStatus;
import pl.stendbeck.taxhelper.model.TaxType;
import pl.stendbeck.taxhelper.model.entity.*;
import pl.stendbeck.taxhelper.repository.InvoiceRepository;
import pl.stendbeck.taxhelper.repository.MonthRepository;
import pl.stendbeck.taxhelper.repository.TaxRepository;
import pl.stendbeck.taxhelper.repository.ZusContributionRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.List;

import static pl.stendbeck.taxhelper.model.ChargeStatus.CLOSED;
import static pl.stendbeck.taxhelper.model.ChargeStatus.OPEN;
import static pl.stendbeck.taxhelper.model.InvoiceType.INCOME;
import static pl.stendbeck.taxhelper.model.TaxType.INCOME_TAX;
import static pl.stendbeck.taxhelper.model.TaxType.VAT_TAX;

@Service
@Transactional
@SuppressWarnings("PMD.TooManyStaticImports")
public class TaxServiceImpl implements TaxService {

    @Value("${tax.term-day-of-month}")
    private int taxTermDayOfMonth;

    @Value("${vat.term-day-of-month}")
    private int vatTermDayOfMonth;

    @Value("${tax.first-tax-threshold}")
    private BigDecimal firstTaxThreshold;

    @Autowired
    private TaxRepository taxRepository;

    @Autowired
    private InvoiceRepository invoiceRepository;

    @Autowired
    private MonthRepository monthRepository;

    @Autowired
    private ZusContributionRepository zusContributionRepository;

    @Override
    public void createTaxesForMonth(Customer customer, Month month) {
        createTax(customer, month, INCOME_TAX);
        createTax(customer, month, VAT_TAX);
    }

    private void createTax(Customer customer, Month month, TaxType type) {
        Tax tax = new Tax();
        tax.setCustomer(customer);
        tax.setMonth(month);
        tax.setStatus(OPEN);
        tax.setType(type);
        tax.setTermDate(calculateTermDate(month, type));
        taxRepository.save(tax);
    }

    @Override
    public void calculateMonth(Month month, boolean closeMonth) {

//        Od kwoty przychodu od początku roku odjąć koszty od początku roku - otrzymamy kwotę dochodu.

        IncomeTaxType taxType = month.getYear().getTaxType();

        BigDecimal monthBase = BigDecimal.ZERO;
        BigDecimal monthVat;

        BigDecimal sprz = BigDecimal.ZERO;
        BigDecimal zak = BigDecimal.ZERO;


        List<Invoice> invoices = invoiceRepository.findByMonthId(month.getId());
        for (Invoice invoice : invoices) {
            if (invoice.getType() == INCOME) {
                monthBase = monthBase.add(invoice.getTaxBase());
                sprz = sprz.add(invoice.getVat());
            } else {
                monthBase = monthBase.subtract(invoice.getTaxBase());
                zak = zak.add(invoice.getVat());
            }
        }

        System.out.println("============ sprzedaż: " + sprz);
        System.out.println("============ zakup: " + zak);

        sprz = sprz.setScale(0, RoundingMode.HALF_UP);
        zak = zak.setScale(0, RoundingMode.HALF_UP);

        monthVat = sprz.subtract(zak);
        System.out.println("============ vat miesięczny: " + monthVat);
        System.out.println("dochód miesięczny: " + monthBase);

        BigDecimal temp = monthRepository.findTaxBaseForYeard(month.getYear().getId()).orElse(BigDecimal.ZERO);

        System.out.println("dochód z poprzednich miesięcy " + temp);

        // dochód:
        BigDecimal totalBase = monthBase.add(temp);
        BigDecimal totalVat = monthVat.add(monthRepository.findVatAmountForYeard(month.getYear().getId()).orElse(BigDecimal.ZERO));

        System.out.println("============ vat totalny: " + totalVat);


        System.out.println("dochód RAZEM: " + totalBase);


//        Od kwoty dochodu odjąć możliwą do rozliczenia stratę z lat ubiegłych oraz sumę zapłaconych składek na ubezpieczenie
//        społeczne w danym roku podatkowym (o ile nie były zaliczane do kosztów w KPiR).
//        Otrzymamy dochód do opodatkowania, który zaokrąglamy do pełnych złotych.


        List<ZusContribution> contributions = zusContributionRepository.findByPaymentDateAndStatus(month.getYear().getValue());


        System.out.println("ilość składek: " + contributions.size());


        BigDecimal socialResult = contributions.stream()
                .map(ZusContribution::getSocialAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("socialne: " + socialResult);

        totalBase = totalBase.subtract(socialResult).setScale(0, RoundingMode.HALF_UP);

        System.out.println("dochód - socialne: " + totalBase);


//        Pomnożyć podstawę opodatkowania przez 17% stawkę podatku do 85 528 zł dochodu.

        BigDecimal taxAmount = taxType.calculateTax(totalBase, firstTaxThreshold);

        System.out.println("obliczony podatek: " + taxAmount);

//        Od podatku odjąć sumę opłaconych w danym roku składek na ubezpieczenie zdrowotne (7,75% od podstawy, nie cała zapłacona składka)
//        - otrzymujemy podatek należny za cały rok.


        BigDecimal healthResult = contributions.stream()
                .map(ZusContribution::getHealthCareAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("zdrowotna przed przekształceniami: " + healthResult);

        BigDecimal zdrowotna = healthResult.divide(new BigDecimal("9"), 2, RoundingMode.HALF_UP);
        zdrowotna = zdrowotna.multiply(new BigDecimal("7.75").setScale(2, RoundingMode.HALF_UP));

        System.out.println("zdrowotna po przekształceniach: " + zdrowotna);

        taxAmount = taxAmount.subtract(zdrowotna);


        System.out.println("podatek - zdrowotna: " + taxAmount);

//        Od kwoty podatku należnego od początku roku należy odjąć zaliczki zapłacone w poprzednich miesiącach
//        oraz kwotę zmniejszającą podatek - uzyskujemy kwotę podatku za dany okres rozliczeniowy.

        List<Tax> vatTaxes = taxRepository.findByYearVat(month.getYear().getId());
        List<Tax> incomeTaxes = taxRepository.findByYearIncome(month.getYear().getId());


        BigDecimal allVat = vatTaxes.stream()
                .map(Tax::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        BigDecimal allIncome = incomeTaxes.stream()
                .map(Tax::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        System.out.println("ilość zapłaconych zaliczek: " + incomeTaxes.size());
        System.out.println("suma zapłaconych zaliczek: " + allIncome);

        System.out.println("========ilość zapłaconych zaliczek: " + vatTaxes.size());
        System.out.println("===========suma zapłaconych zaliczek: " + allVat);


        if (totalBase.compareTo(firstTaxThreshold) < 0) {
            taxAmount = taxAmount.subtract(new BigDecimal("556.02"));
        }


        System.out.println("podatek - kwota wolna: " + taxAmount);


        taxAmount = taxAmount.subtract(allIncome).setScale(0, RoundingMode.HALF_UP);
        System.out.println("podatek - zaliczki: " + taxAmount);


        Tax incomeTax = taxRepository.findByMonthIdAndType(month.getId(), INCOME_TAX);
        incomeTax.setAmount(taxAmount.signum() > 0 ? taxAmount : BigDecimal.ZERO);

//        Uwaga: Po zmianie przepisów, od 2017 roku, po przekroczeniu pierwszego progu podatkowego (dochód powyżej 85. 528 zł) na etapie obliczania zaliczki na podatek dochodowy nie uwzględnia się kwoty wolnej od podatku.
//        Zaliczkę za dany okres rozliczeniowy zaokrąglamy do pełnych złotych - uzyskujemy kwotę podlegającą wpłacie do urzędu skarbowego.
//
//        Zobacz więcej: https://poradnikprzedsiebiorcy.pl/-jak-obliczyc-podatek-dochodowy


        // STAROSC


        //poodejmować zaliczki


        //zapisz podatki do bazy
        Tax vatTax = taxRepository.findByMonthIdAndType(month.getId(), VAT_TAX);
        BigDecimal ggg = totalVat.subtract(allVat).setScale(0, RoundingMode.HALF_UP);
        vatTax.setAmount(ggg.signum() > 0 ? ggg : BigDecimal.ZERO);


        if (closeMonth) {
            vatTax.setStatus(CLOSED);
            incomeTax.setStatus(CLOSED);

            month.setVatAmount(monthVat);
            month.setTaxBase(monthBase);
            month.setStatus(PeriodStatus.CLOSED);
        }
    }


    public LocalDate calculateTermDate(Month month, TaxType type) {
        if (type == VAT_TAX) {
            return month.getDateFrom().plusMonths(1L).withDayOfMonth(vatTermDayOfMonth);
        } else if (type == INCOME_TAX) {
            int modulo = month.getDateFrom().getMonthValue() % 3;
            long result;
            switch (modulo) {
                case 1:
                    result = 3L;
                    break;
                case 2:
                    result = 2L;
                    break;
                case 0:
                    result = 1L;
                    break;
                default:
                    result = 0L;
            }
            return month.getDateFrom().plusMonths(result).withDayOfMonth(taxTermDayOfMonth);
        }
        return LocalDate.now();
    }
}
