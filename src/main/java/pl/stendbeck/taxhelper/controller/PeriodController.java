package pl.stendbeck.taxhelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.stendbeck.taxhelper.model.vo.CreateMonthVO;
import pl.stendbeck.taxhelper.service.PeriodService;

import javax.validation.Valid;

@RestController
public class PeriodController {

    @Autowired
    private PeriodService service;

    @PostMapping("/v1/month")
    public ResponseEntity<Void> createMonth(@RequestBody @Valid CreateMonthVO monthVO) {
        service.createMonth(monthVO);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/v1/month/{monthId}/close")
    public ResponseEntity<Void> closeMonth(@PathVariable long monthId) {
        service.closeMonth(monthId);
        return ResponseEntity.ok().build();
    }
}
