package pl.stendbeck.taxhelper.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.stendbeck.taxhelper.model.vo.InvoiceVO;
import pl.stendbeck.taxhelper.service.InvoiceService;

@RestController
public class InvoiceController {

    @Autowired
    private InvoiceService service;

    @PostMapping("/v1/invoice")
    public void addInvoice(@RequestBody(required = true) InvoiceVO invoice) {
        service.addInvoice(invoice);
    }
}
