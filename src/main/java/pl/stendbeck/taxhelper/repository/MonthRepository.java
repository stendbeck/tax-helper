package pl.stendbeck.taxhelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.stendbeck.taxhelper.model.entity.Month;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface MonthRepository extends JpaRepository<Month, Long> {

    Month findByName(String name);

    @Query("SELECT m " +
            "FROM Month m " +
            "WHERE dateFrom <= :date " +
            "AND dateTo >= :date " +
            "AND status = 'ACTIVE'")
    Optional<Month> findByDate(LocalDate date);

//    @Query("SELECT m " +
//            "FROM Month m " +
//            "LEFT JOIN m.year y " +
//            "WHERE y.id = :yearId " +
//            "AND m.status = 'CLOSED'")
//    List<Month> findAllClosedMonthsFromYear(long yearId);

    @Query("SELECT SUM(m.vatAmount)" +
            " FROM Month m" +
            " LEFT JOIN m.year y" +
            " WHERE y.id = :yearId" +
            " AND m.status = 'CLOSED'")
    Optional<BigDecimal> findVatAmountForYeard(long yearId);

    @Query("SELECT SUM(m.taxBase)" +
            " FROM Month m" +
            " LEFT JOIN m.year y" +
            " WHERE y.id = :yearId" +
            " AND m.status = 'CLOSED'")
    Optional<BigDecimal> findTaxBaseForYeard(long yearId);
}
