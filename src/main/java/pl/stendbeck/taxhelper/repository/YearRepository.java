package pl.stendbeck.taxhelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.stendbeck.taxhelper.model.entity.Year;

@Repository
public interface YearRepository extends JpaRepository<Year, Long> {

    Year findByValue(int value);

}