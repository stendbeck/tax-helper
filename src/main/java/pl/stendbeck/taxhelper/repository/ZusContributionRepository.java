package pl.stendbeck.taxhelper.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.stendbeck.taxhelper.model.entity.ZusContribution;

import java.util.List;

@Repository
public interface ZusContributionRepository extends JpaRepository<ZusContribution, Long> {

    @Query("SELECT z " +
            "FROM ZusContribution z " +
            "WHERE YEAR(paymentDate) = :year " +
            "AND status = 'PAID'")
    List<ZusContribution> findByPaymentDateAndStatus(int year);


}
