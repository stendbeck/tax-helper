---------------------------------------------------
-- Add customer table
---------------------------------------------------
CREATE SEQUENCE seq_customer;

CREATE TABLE customer
(
    id          NUMERIC(19)     NOT NULL,
    login       VARCHAR(30)     NOT NULL,
    name        VARCHAR(255)    NOT NULL
);

ALTER TABLE customer ADD PRIMARY KEY (id);
CREATE INDEX idx_customer_login on customer(login);

---------------------------------------------------
-- Add year table
---------------------------------------------------
CREATE SEQUENCE seq_year;

CREATE TABLE year
(
    id          NUMERIC(19) NOT NULL,
    customer_id NUMERIC(19) NOT NULL,
    value       NUMERIC(4)  NOT NULL,
    tax_type    VARCHAR(20) NOT NULL,
    status      VARCHAR(20) NOT NULL
);

ALTER TABLE year ADD PRIMARY KEY (id);
CREATE INDEX idx_year_customer_id on year(customer_id);

---------------------------------------------------
-- Add month table
---------------------------------------------------
CREATE SEQUENCE seq_month;

CREATE TABLE month
(
    id          NUMERIC(19)     NOT NULL,
    customer_id NUMERIC(19)     NOT NULL,
    year_id     NUMERIC(19)     NOT NULL,
    name        VARCHAR(100)    NOT NULL,
    date_from   DATE            NOT NULL,
    date_to     DATE            NOT NULL,
    tax_base    NUMERIC(7,2)    NOT NULL,
    vat_amount  NUMERIC(7,2)    NOT NULL,
    status      VARCHAR(20)     NOT NULL
);

ALTER TABLE month ADD PRIMARY KEY (id);
CREATE INDEX idx_month_customer_id on month(customer_id);
ALTER TABLE month ADD CONSTRAINT fk_month_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id);
ALTER TABLE month ADD CONSTRAINT fk_month_year_id FOREIGN KEY (year_id) REFERENCES year(id);

---------------------------------------------------
-- Add invoice table
---------------------------------------------------
CREATE SEQUENCE seq_invoice;

CREATE TABLE invoice
(
    id              NUMERIC(19)     NOT NULL,
    customer_id     NUMERIC(19)     NOT NULL,
    month_id        NUMERIC(19)     NOT NULL,
    date            DATE            NOT NULL,
    amount          NUMERIC(7,2)    NOT NULL,
    type            VARCHAR(20)     NOT NULL,
    vat_value        VARCHAR(20)     NOT NULL,
    tax_value        VARCHAR(20)     NOT NULL,
    description     VARCHAR(255)    NOT NULL
);

ALTER TABLE invoice ADD PRIMARY KEY (id);
ALTER TABLE invoice ADD CONSTRAINT fk_invoice_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id);
ALTER TABLE invoice ADD CONSTRAINT fk_invoice_month_id FOREIGN KEY (month_id) REFERENCES month(id);
CREATE INDEX idx_invoice_customer on invoice(customer_id);
CREATE INDEX idx_invoice_month on invoice(month_id);
CREATE INDEX idx_invoice_customer_date on invoice(customer_id, date);

---------------------------------------------------
-- Add tax table
---------------------------------------------------
CREATE SEQUENCE seq_tax;

CREATE TABLE tax
(
    id              NUMERIC(19)     NOT NULL,
    customer_id     NUMERIC(19)     NOT NULL,
    month_id        NUMERIC(19)     NOT NULL,
    payment_date    DATE,
    term_date       DATE            NOT NULL,
    type            VARCHAR(20)     NOT NULL,
    status          VARCHAR(20)     NOT NULL,
    amount          NUMERIC(7,2)
);

ALTER TABLE tax ADD PRIMARY KEY (id);
ALTER TABLE tax ADD CONSTRAINT fk_tax_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id);
ALTER TABLE tax ADD CONSTRAINT fk_tax_month_id FOREIGN KEY (month_id) REFERENCES month(id);
CREATE INDEX idx_tax_month on tax(month_id);

---------------------------------------------------
-- Add zus_contribution table
---------------------------------------------------
CREATE SEQUENCE seq_zus_contribution;

CREATE TABLE zus_contribution
(
    id                  NUMERIC(19) NOT NULL,
    customer_id         NUMERIC(19) NOT NULL,
    month_id            NUMERIC(19) NOT NULL,
    payment_date        DATE,
    term_date           DATE        NOT NULL,
    status              VARCHAR(20) NOT NULL,
    social_amount       NUMERIC(7,2),
    health_care_amount  NUMERIC(7,2),
    labor_fund_amount   NUMERIC(7,2)
);

ALTER TABLE zus_contribution ADD PRIMARY KEY (id);
ALTER TABLE zus_contribution ADD CONSTRAINT fk_zus_contribution_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id);
ALTER TABLE zus_contribution ADD CONSTRAINT fk_zus_contribution_month_id FOREIGN KEY (month_id) REFERENCES month(id);
CREATE INDEX idx_zus_contribution_month on tax(month_id);
